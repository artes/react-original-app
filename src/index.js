import React from 'react'
import ReactDOM from 'react-dom'
import './assets/css/index.css'
import TheHeader from './TheHeader'
import ToDoList from './ToDoList'
import NameForm from './NameForm'
import Calculator from './Calculator'
import * as serviceWorker from './serviceWorker'

ReactDOM.render(
  <React.StrictMode>
    <TheHeader />
    <div className="wrapper-outer">
      <div className="wrapper-inner">
        <div className="module-row">
          <ToDoList />
          <NameForm />
          <Calculator />
        </div>
        <div className="module-row">

        </div>
      </div>
    </div>
  </React.StrictMode>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
