import React from 'react'
import { dateFormat } from './assets/js/utils/dateFormat'
import './assets/css/header.css'
import ReactLogo from './assets/images/react-logo.svg'

class TheHeader extends React.Component {
  constructor(props) {
    super(props)
    // props is from parent component, and state is itself state
    this.state = { date: new Date() }
  }

  componentDidMount() {
    this.timer = setInterval(() => this.tick(), 1000)
  }

  componentWillUnmount() {
    clearInterval(this.timer)
  }

  tick() {
    // schedule a ui update
    // the three things about state
    // 1. do not modify state directly, and the only place where you can assign this.state is the constructor
    // 2. state updates maybe asynchronous
    // 3. state updates are merged
    this.setState({
      date: new Date()
    })
  }

  render() {
    return (
      <header className="app-header">
        <div className="header-inner">
          <div className="header-left">
            <img className="header-logo" src={ReactLogo} alt="" />
            <span className="header-title">React</span>
          </div>
          <div className="header-right">
            <div className="header-clock">Now is {dateFormat(this.state.date, 'yyyy-MM-dd hh:mm:ss')}.</div>
          </div>
        </div>
      </header>
    )
  }
}

export default TheHeader
