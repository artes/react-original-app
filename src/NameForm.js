import React from 'react'

class NameForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      nameForm: {
        name: '',
        essay: '',
        flavor: '',
        isGoing: false,
        numberOfGuests: ''
      }
    }

    this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleChange(event, label) {
    const target = event.target
    const booleanElementList = ['checkbox', 'radio']
    const value = booleanElementList.includes(target.type) ? target.checked : target.value
    const nameForm = this.state.nameForm
    nameForm[label] = value
    this.setState({ nameForm: nameForm })
  }

  handleSubmit(event) {
    console.log('A name was submitted: ', this.state.nameForm)
    event.preventDefault()
  }

  render() {
    return (
      <form className="module-container" onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" value={this.state.nameForm.name} onChange={(e) => this.handleChange(e, 'name')} />
        </label>
        <br />
        <label>
          Essay:
          <textarea value={this.state.nameForm.essay} onChange={(e) => this.handleChange(e, 'essay')} />
        </label>
        <br />
        <label>
          Pick your favorite flavor:
          <select value={this.state.nameForm.flavor} onChange={(e) => this.handleChange(e, 'flavor')}>
            <option value="grapefruit">Grapefruit</option>
            <option value="lime">Lime</option>
            <option value="coconut">Coconut</option>
            <option value="mango">Mango</option>
          </select>
        </label>
        <br />
        <label>
          Is going:
          <input
            name="isGoing"
            type="checkbox"
            checked={this.state.nameForm.isGoing}
            onChange={(e) => this.handleChange(e, 'isGoing')} />
        </label>
        <br />
        <label>
          Number of guests:
          <input
            name="numberOfGuests"
            type="text"
            value={this.state.nameForm.numberOfGuests}
            onChange={(e) => this.handleChange(e, 'numberOfGuests')} />
        </label>
        <br />
        <input type="submit" value="Submit" />
      </form>
    )
  }
}

export default NameForm
